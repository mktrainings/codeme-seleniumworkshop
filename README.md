# Codeme-SeleniumWorkshop

Repozytorium na warsztaty Selenium 27.04.2020

## Opis ćwiczeń i dodatkowe materiały na wiki projektu

https://gitlab.com/mktrainings/codeme-seleniumworkshop/-/wikis/home

## Pliki

* **workshop.html** - projekt do użycia w *Katalon Recorder* z wykonywanymi ćwiczeniami
* **miasta.csv** - przykładowy plik z danymi o miastach. **Uwaga!** trzeba pamiętać, żeby po załadowaniu pliku projektu załadować również ten plik jako źródło danych